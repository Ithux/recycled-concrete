local S
if minetest.get_modpath("intllib") then
	S = intllib.Getter()
else
	S = function(s) return s end
end


minetest.register_node("recycled_concrete:recycled_concrete", {
	description = S("recycled_concrete"),
	tiles = {"recycled_concrete.png"},
	groups = {cracky=3},
})

minetest.register_node("recycled_concrete:recycled_concrete_stairs", {
    description = "recycled_concrete_stair",
    tiles = {"recycled_concrete.png"},
    paramtype2 = "facedir",
    groups = {cracky = 3},
    drawtype = "nodebox",
    node_box = {
        type = "fixed",
        fixed = {
            {-0.5, -0.5, -0.5, 0.5, 0, 0.5},
            {-0.5, 0, 0, 0.5, 0.5, 0.5}, 
        },
    },
})
